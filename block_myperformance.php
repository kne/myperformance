<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * My performance block
 *
 * @package    block_myperformance
 * @copyright  2020 KNE | Knowledge Networks Engineering (https://kne.it)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

// Required lib.
require_once($CFG->dirroot . '/blocks/myperformance/lib.php');

/**
 * My performance block class
 *
 * @package    block_myperformance
 * @copyright  2020 KNE | Knowledge Networks Engineering (https://kne.it)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_myperformance extends block_base {

    /**
     * Set the initial properties for the block
     */
    public function init() {
        $this->title = get_string('myperformance', 'block_myperformance') . " "  . date("m/Y");
    }

    /**
     * Gets the content for this block
     */
    public function get_content() {
        global $USER, $COURSE;

        if ($this->content !== null) {
            return $this->content;
        }

        $performance = get_user_performance($USER->id, $COURSE->id, date("Y-m"), $this->config);
        if(!$performance[performancedata]) $points = " - ";
        else $points = $performance[performancedata];
        if(!$performance[rank]) $rank = " - ";
        else $rank = $performance[rank];

        $content = html_writer::tag('span', get_string('mypoints', 'block_myperformance') . ": " .
                   html_writer::tag('span', " " . $points . " ", array('class' => 'badge-info')),
                   array('class' => 'badge'));
        $content .= html_writer::empty_tag('br');
        $content .= html_writer::tag('span', get_string('myrank', 'block_myperformance') . ": " .
                    html_writer::tag('span', " " . $rank . " ", array('class' => 'badge-success')),
                    array('class' => 'badge'));

        $this->content = new stdClass;
        $this->content->text = $content;

        return $this->content;
    }
}