<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Edit form
 *
 * @package    block_myperformance
 * @copyright  2020 KNE | Knowledge Networks Engineering (https://kne.it)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Block edit form class
 *
 * @package    block_myperformance
 * @copyright  2020 KNE | Knowledge Networks Engineering (https://kne.it)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_myperformance_edit_form extends block_edit_form {
    /**
     * Set form elements
     * @param form $mform Config form
     */
    protected function specific_definition($mform) {
        $mform->addElement('header', 'config_header', get_string('blocksettings', 'block'));

        // DB host.
        $mform->addElement('text', 'config_dbhost', get_string('dbhost', 'block_myperformance'));
        $mform->setDefault('config_dbhost', 'localhost');
        $mform->setType('config_dbhost', PARAM_RAW);
        $mform->addRule('config_dbhost', get_string('error', 'block_myperformance'), 'required', '', 'server', false, false);

        // DB name.
        $mform->addElement('text', 'config_dbname', get_string('dbname', 'block_myperformance'));
        $mform->setDefault('config_dbname', '');
        $mform->setType('config_dbname', PARAM_RAW);
        $mform->addRule('config_dbname', get_string('error', 'block_myperformance'), 'required', '', 'server', false, false);

        // DB user.
        $mform->addElement('text', 'config_dbuser', get_string('dbuser', 'block_myperformance'));
        $mform->setDefault('config_dbuser', '');
        $mform->setType('config_dbuser', PARAM_RAW);
        $mform->addRule('config_dbuser', get_string('error', 'block_myperformance'), 'required', '', 'server', false, false);

        // DB password.
        $mform->addElement('password', 'config_dbpassword', get_string('dbpassword', 'block_myperformance'));
        $mform->setDefault('config_dbpassword', '');
        $mform->addRule('config_dbpassword', get_string('error', 'block_myperformance'), 'required', '', 'server', false, false);

        // Performance table.
        $mform->addElement('text', 'config_performancetable', get_string('performancetable', 'block_myperformance'));
        $mform->setDefault('config_performancetable', '');
        $mform->setType('config_performancetable', PARAM_RAW);
        $mform->addRule('config_performancetable', get_string('error', 'block_myperformance'),
                        'required', '', 'server', false, false);

        // Data mapping fields.
        $mform->addElement('header', 'config_datamapping', get_string('datamapping', 'block_myperformance'));

        // User id field.
        $mform->addElement('text', 'config_userid', get_string('userid', 'block_myperformance'));
        $mform->setDefault('config_userid', 'userid');
        $mform->setType('config_userid', PARAM_RAW);
        $mform->addRule('config_userid', get_string('error', 'block_myperformance'), 'required', '', 'server', false, false);

        // Course id field.
        $mform->addElement('text', 'config_courseid', get_string('courseid', 'block_myperformance'));
        $mform->setDefault('config_courseid', 'courseid');
        $mform->setType('config_courseid', PARAM_RAW);
        $mform->addRule('config_courseid', get_string('error', 'block_myperformance'), 'required', '', 'server', false, false);

        // Period field.
        $mform->addElement('text', 'config_period', get_string('period', 'block_myperformance'));
        $mform->setDefault('config_period', 'period');
        $mform->setType('config_period', PARAM_RAW);
        $mform->addRule('config_period', get_string('error', 'block_myperformance'), 'required', '', 'server', false, false);

        // Performace data field.
        $mform->addElement('text', 'config_performancedata', get_string('performancedata', 'block_myperformance'));
        $mform->setDefault('config_performancedata', 'performancedata');
        $mform->setType('config_performancedata', PARAM_RAW);
        $mform->addRule('config_performancedata', get_string('error', 'block_myperformance'),
                        'required', '', 'server', false, false);

        // Rank field.
        $mform->addElement('text', 'config_rank', get_string('rank', 'block_myperformance'));
        $mform->setDefault('config_rank', 'rank');
        $mform->setType('config_rank', PARAM_RAW);
        $mform->addRule('config_rank', get_string('error', 'block_myperformance'), 'required', '', 'server', false, false);
    }
}