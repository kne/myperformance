<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings - EN
 *
 * @package    block_myperformance
 * @copyright  2020 KNE | Knowledge Networks Engineering (https://kne.it)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'My performance';
$string['myperformance'] = 'My monthly performance';
$string['myperformance:addinstance'] = 'Add a new My performance block';
$string['error'] = 'Required data!';

$string['dbhost'] = 'Database host';
$string['dbname'] = 'Database name';
$string['dbuser'] = 'Database user';
$string['dbpassword'] = 'Database password';
$string['performancetable'] = 'Performance table';

$string['datamapping'] = 'Data mapping';
$string['userid'] = 'User id field';
$string['courseid'] = 'Course id field';
$string['period'] = 'Period field';
$string['performancedata'] = 'Performance field';
$string['rank'] = 'Rank field';

$string['dbcantconnect'] = 'Database connection error. Please contact system admin.';

$string['mypoints'] = 'Selling points';
$string['myrank'] = 'Ranking position';