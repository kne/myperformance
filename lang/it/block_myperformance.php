<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings - IT
 *
 * @package    block_myperformance
 * @copyright  2020 KNE | Knowledge Networks Engineering (https://kne.it)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Le mie performance';
$string['myperformance'] = 'Le mie performance mensili';
$string['myperformance:addinstance'] = 'Aggiungi un nuovo blocco Le mie performance';
$string['error'] = 'Dato richiesto!';

$string['dbhost'] = 'Database host';
$string['dbname'] = 'Database name';
$string['dbuser'] = 'Database user';
$string['dbpassword'] = 'Database password';
$string['performancetable'] = 'Tabella delle performance';

$string['datamapping'] = 'Mappatura dati';
$string['userid'] = 'Riferimento codice utente';
$string['courseid'] = 'Riferimento codice corso';
$string['period'] = 'Riferimento periodo';
$string['performancedata'] = 'Performance del periodo';
$string['rank'] = 'Posizione del periodo';

$string['dbcantconnect'] = 'Errore di connessione al database. Contattare l\'amministratore.';

$string['mypoints'] = 'Punti di vendita';
$string['myrank'] = 'Posizione in classifica';