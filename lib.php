<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block lib
 *
 * @package    block_myperformance
 * @copyright  2020 KNE | Knowledge Networks Engineering (https://kne.it)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/adodb/adodb.inc.php');

/**
 * Get user performance for course and period
 *
 * @package block_myperformance
 *
 * @param int $userid
 * @param int $courseid
 * @param string $period
 * @param array $config
 * @return array
 */
function get_user_performance($userid, $courseid, $period, $config) {
    $performace = array();

    $performancedb = db_init($config);

    $rs = $performancedb->Execute("SELECT *
                                     FROM {$config->performancetable}
                                    WHERE {$config->userid} = $userid
                                          AND {$config->courseid} = $courseid
                                          AND period = '$period'
                                    LIMIT 1");

    if (!$rs) {
        $performancedb->Close();
        debugging(get_string('dbcantconnect', 'block_myperformance'));
        return false;
    }

    if ($rs->EOF) {
        $performancedb->Close();
        return array();
    }

    $performace = $rs->FetchRow();
    $rs->Close();

    return $performace;
}

/**
 * Connect to external database (forcing new connection).
 *
 * @param array $config
 * @return ADOConnection
 */
function db_init($config) {
    $conn = ADONewConnection('mysqli');
    $conn->Connect($config->dbhost, $config->dbuser, $config->dbpassword, $config->dbname, true);
    $conn->SetFetchMode(ADODB_FETCH_ASSOC);

    return $conn;
}